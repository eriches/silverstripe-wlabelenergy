<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ORM\DB;
use SilverStripe\Control\Email\Email;
use Hestec\WlabelEnergy\WlabelEnergySubscription;
use Hestec\WlabelEnergy\WlabelEnergySupplier;
use SilverStripe\Core\Config\Config;

class WlabelEnergyCron extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'UpdateWlabelEnergy',
        'test'
    );

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            /*$whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://api-internet.whitelabeled.nl/v1/compare/'.$whitelabel_id.'?include_tv=true&include_phone=true&page=1&items_per_page=1', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);
            $msgs = json_decode($response->getBody());


            foreach ($msgs->products as $node) {

                //echo $node->score_elements;

                foreach ($node->score_elements as $node) {

                    echo $node->type;

                }

            }*/

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function UpdateWlabelEnergy() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            $whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', 'https://api-energie.whitelabeled.nl/v1/compare/' . $whitelabel_id . '?page=1&items_per_page=999', [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ]
                ]);

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Whitelabeled Energy", $e->getMessage());
                $email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody());

                DB::query("UPDATE WlabelEnergySubscription SET StillInApi = 0");

                foreach ($msgs->products as $node) {

                    $count = WlabelEnergySubscription::get()->filter('BaseId', $node->base_id)->count();

                    if ($count == 0) {

                        $sub = new WlabelEnergySubscription();

                    } else {

                        $sub = WlabelEnergySubscription::get()->filter('BaseId', $node->base_id)->first();

                    }

                    // if the base_name starts with the provider_name, remove it
                    $basename = $this->removeProviderName($node->base_name, $node->provider_name);

                    $sub->UrlId = SiteTree::create()->generateURLSegment($node->provider_name . "-" . $basename . "-" . $node->base_id);
                    $sub->ShortUrlId = SiteTree::create()->generateURLSegment($node->base_id . "-" . $basename);
                    $sub->StillInApi = 1; // see the DB::query above
                    $sub->BaseId = $node->base_id;
                    $sub->BaseName = $basename;
                    $sub->Provider = $node->provider;
                    $sub->ProviderName = $node->provider_name;
                    $sub->OfferType = $node->offer_type;
                    $sub->OfferTextLong = $node->offer_text_long;
                    $sub->OfferTextShort = $node->offer_text_short;
                    $sub->TotalDiscount = $node->total_discount;
                    $sub->ComparePrice = $node->compare_price;
                    $sub->ContractDuration = $node->contract_duration;
                    $sub->ContractDurationText = $node->contract_duration_text;
                    $sub->ElectricityIncluded = $node->electricity_included;
                    $sub->ElectricityGreen = $node->electricity_green;
                    $sub->ElectricitySource = $node->electricity_source;
                    $sub->GasIncluded = $node->gas_included;
                    $sub->GasGreen = $node->gas_green;
                    $sub->AvailableWithoutGas = $node->available_without_gas;
                    $sub->SignupUrl = $node->signup_url;

                    $votes = rand(0, 2);
                    $score = rand(7, 10) * $votes;
                    $sub->RatingVotes = $sub->RatingVotes + $votes;
                    $sub->RatingScore = $sub->RatingScore + $score;

                    if ($supplier = WlabelEnergySupplier::get()->filter('SystemName', $node->provider)->first()) {

                        $sub->WlabelEnergySupplierID = $supplier->ID;

                    } else {

                        $supplier = new WlabelEnergySupplier();
                        $supplier->SystemName = $node->provider;
                        $supplier->Name = $node->provider_name;
                        $supplier->write();

                        $sub->WlabelEnergySupplierID = $supplier->ID;

                        $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "New supplier added for Whitelabeled Energy", "The supplier ".$node->provider_name." is added for Whitelabeled Energy");
                        $email->sendPlain();

                    }

                    $sub->write();

                }
            }

            return "updated";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

    public function removeProviderName($string, $startString){

        $len = strlen($startString);
        if (substr(strtolower($string), 0, $len) === strtolower($startString)){
            return substr($string, $len+1);
        }
        return $string;

    }

}
