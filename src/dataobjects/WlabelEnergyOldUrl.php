<?php

namespace Hestec\WlabelEnergy;

use SilverStripe\ORM\DataObject;

class WlabelEnergyOldUrl extends DataObject {

    private static $singular_name = 'WlabelEnergyOldUrl';
    private static $plural_name = 'WlabelEnergyOldUrls';

    private static $table_name = 'WlabelEnergyOldUrl';

    private static $db = array(
        'UrlId' => 'Varchar(255)',
        'ShortUrlId' => 'Varchar(255)'
    );

    private static $has_one = array(
        'WlabelEnergySubscription' => WlabelEnergySubscription::class
    );

}