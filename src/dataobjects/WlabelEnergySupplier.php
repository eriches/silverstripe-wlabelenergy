<?php

namespace Hestec\WlabelEnergy;

use SilverStripe\ORM\DataObject;

class WlabelEnergySupplier extends DataObject {

    private static $singular_name = 'WlabelEnergySupplier';
    private static $plural_name = 'WlabelEnergySuppliers';

    private static $table_name = 'WlabelEnergySupplier';

    private static $db = array(
        'SystemName' => 'Varchar(50)',
        'Name' => 'Varchar(50)'
    );

    /*private static $has_one = array(
        'SupplierPage' => SupplierPage::class
    );*/

    private static $has_many = array(
        'WlabelEnergySubscriptions' => WlabelEnergySubscription::class
    );

    /*private static $summary_fields = array(
        'Title',
        'StartDate.Nice',
        'EndDate.Nice',
        'Enabled.Nice'
    );*/

}