<?php

namespace Hestec\WlabelEnergy;

use SilverStripe\ORM\DataObject;

class WlabelEnergySubscription extends DataObject {

    private static $singular_name = 'WlabelEnergySubscription';
    private static $plural_name = 'WlabelEnergySubscriptions';

    private static $table_name = 'WlabelEnergySubscription';

    private static $db = array(
        'BaseId' => 'Int',
        'BaseName' => 'Varchar(255)',
        'UrlId' => 'Varchar(255)',
        'ShortUrlId' => 'Varchar(255)',
        'Provider' => 'Varchar(50)',
        'ProviderName' => 'Varchar(50)',
        'OfferType' => 'Varchar(20)',
        'OfferTextLong' => 'Text',
        'OfferTextShort' => 'Text',
        'TotalDiscount' => 'Currency',
        'ComparePrice' => 'Currency',
        'ContractDuration' => 'Int',
        'ContractDurationText' => 'Varchar(20)',
        'ElectricityIncluded' => 'Boolean',
        'ElectricityGreen' => 'Varchar(20)',
        'ElectricitySource' => 'MultiValueField',
        'GasIncluded' => 'Boolean',
        'GasGreen' => 'Boolean',
        'AvailableWithoutGas' => 'Boolean',
        'RatingVotes' => 'Int',
        'RatingScore' => 'Int',
        'SignupUrl' => 'Varchar(255)',
        'StillInApi' => 'Boolean',
        'ProductPage' => 'Boolean'
    );

    private static $has_one = array(
        //'ComparisonAllInOnePage' => ComparisonAllInOnePage::class,
        'WlabelEnergySupplier' => WlabelEnergySupplier::class
    );

    public function MbDownloadSpeed(){

        $mb = round($this->DownloadSpeed / 1000);
        return floor($mb/5) * 5;

    }

    public function MbUploadSpeed(){

        $mb = round($this->UploadSpeed / 1000);
        $output = floor($mb/5) * 5;
        if ($mb < 10){
            $output = floor($mb/1) * 1;
        }
        if ($mb < 1){
            $output = $this->UploadSpeed/1000;
        }
        return $output;

    }

    public function PriceEuro($price){

        $output = number_format($price, 2, ',', '');

        return $output;

    }

    public function RatingAverageScore(){

        $output = round($this->RatingScore / $this->RatingVotes, 1);
        return number_format($output, 1, '.', '');

    }

    public function onBeforeWrite()
    {

        if ($this->ID && $this->isChanged('UrlId')){

            $old = WlabelEnergySubscription::get()->byID($this->ID);

            $add = new WlabelEnergyOldUrl();
            $add->UrlId = $old->UrlId;
            $add->ShortUrlId = $old->ShortUrlId;
            $add->WlabelEnergySubscriptionID = $this->ID;
            $add->write();

            $this->ProductPage = 0;

        }

        parent::onBeforeWrite();

    }

}